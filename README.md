# nginx



## Description

This role installing nginx

## Include role

```
- src: https://gitlab.com/ansible_roles_a_whiter/nginx.git
  version: "1.0"
  scm: git
  name: nginx
```


## Example playbook

```
- hosts: nginx
  gather_facts: true
  become: true

  roles:
    - {role: nginx, when: ansible_system == "Linux"}

```
